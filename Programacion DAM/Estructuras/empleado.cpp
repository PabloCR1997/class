#include <stdio.h>
#include <stdlib.h>

struct TEmpleado {
    char *nombre;
    char *apellidos;
    unsigned edad;
    double salario;
};

void alta (struct TEmpleado *nuevo) {

    printf (" Nombre: ");
    scanf (" %s", nuevo->nombre);
    printf (" Apellidos: ");
    scanf (" %s", nuevo->apellidos);
    printf (" Edad: ");
    scanf (" %u", &nuevo->edad);
    printf (" Salario: ");
    scanf (" %lf", &nuevo->salario);

    printf ("\n\n");
}

void imprime_empleado (struct TEmpleado *empleado) {

    printf (" Nombre: %s",    empleado->nombre);
    printf (" Apellidos: %s", empleado->apellidos);
    printf (" Edad: %u",      empleado->edad);
    printf (" Salario: %lf",  empleado->salario);
}

int main (int argc, char *argv[]) {

    unsigned mes, op;
    bool exit = false;
    struct TEmpleado antonio, pedro;
    struct TEmpleado *ptr[] = {&antonio, &pedro};

    printf (" Elija una opci�n: \n"
            "\t (1) Alta empleado\n"
            "\t (2) Ver empleados\n"
            "\t (3) Salir\n"
            "\n\t Selecci�n: ");
    scanf (" %u", &op);

    op--;

    do {
        switch (op) {
            case 0:
                alta(&antonio);
                alta(&pedro);
                break;
            case 1:
                imprime_empleado(&antonio);
                imprime_empleado(&pedro);
                break;
            case 2:
                exit = true;
                break;
            default:
                printf (" Opci�n incorrecta.");
        }

    } while (!exit);

    if (exit) return EXIT_SUCCESS;

    printf (" Empleados: \n"
            "\t (1) Antonio\n"
            "\t (2) Pedro\n"
           );
    printf (" Empleado del mes: ");
    scanf (" %i", &mes);

    printf (" Empleado del mes: ");

    ptr[mes-1]->salario *= 1.01;

    imprime_empleado(ptr[mes-1]);



    return EXIT_SUCCESS;
}

