#include <stdio.h>
#include <stdlib.h>

int fibo(int n){

	if(n == 0 || n == 1){
		
		return 1;
	}else
		
		return fibo (n-1) + fibo (n-2);
}

int main(int argc, char *argv[]){
	
	int n;
	
	printf("Indique un numero: ");
	scanf("%i", &n);
	
	printf("Su proxima secuencia de fibonacci es: ", fibo(n));	
	
	
	
	
}
