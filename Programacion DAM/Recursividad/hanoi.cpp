#include <stdio.h>
#include <stdlib.h>

int T_hanoi(int n, int o, int aux, int d){
	
	if(n > 0){
		T_hanoi(n-1,o,aux,d);
		printf("Se mueve el anillo desde la torre %i hasta la torre %i \n",o,d);
		T_hanoi(n-1,aux,d,o);
	}
	
	
}

int main (int argc, char *argv[]){

	int n;
	printf("Indique el numero de anillos: ");
	scanf("%i",&n);
	
	T_hanoi(n,1,3,2);
	
	
	
}
