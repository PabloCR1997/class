#include <stdio.h>
#include <stdlib.h>

int suma(int num1,int num2){
	
	return num1 + num2;
	
}

int resta(int num1,int num2){
	return num1 - num2;
}

int main (int argc, char *argv[]){
	
	int num1,num2;
	
	int(*fp[])(int ,int) = {suma,resta};
	
	printf("Para sumar pulse 1: \n");
	printf("Para restar pulse 2: \n");
	
	int opt;
	
	scanf("%i",&opt);
	
	if(opt != 1 && opt != 2){
		
		fprintf(stderr,"Datos incorrectos");
		exit(1);
	}
	
	printf("Indique dos numeros enteros: ");
	scanf("%i %i",&num1,&num2);
	
	printf("El total es: %i", (fp[opt - 1])(num1,num2));
	
	
	
	
}

