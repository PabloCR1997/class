#include <stdio.h>
#include <stdlib.h>

#define N 10

int factorial (int n) {
    if ( n <= 0 )
        return 1;

    return n * factorial (n-1);
}

double e (unsigned n) {
    if ( n <= 0 )       
        return 1;
    return 1./factorial (n) + e (n-1);
}

int main (int argc, char *argv[]) {

    printf ("e = %.4lf\n", e (N));

    return EXIT_SUCCESS;
}

