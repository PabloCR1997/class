#include <stdio.h>
#include <stdlib.h>


int factorial(unsigned n){
	
	if (n<=0)
		return 1;
	
	return n * factorial (n-1);
}

int main(int argc, char *argv[]){
	
	unsigned n;
	printf("Indique n: ");
	scanf("%i",&n);
	
	printf("El factorial de %i da %i",n,factorial(n));
	
	return EXIT_SUCCESS;
	
	
}

