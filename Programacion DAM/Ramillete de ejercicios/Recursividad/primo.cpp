#include <stdio.h>
#include <stdlib.h>

#define N 25


int divisores (int n) {

    int static div = 0;

    if ( n < 1 )
        return div;

    if ( N % n == 0 )
        div++;

    return divisores(n-1);
}


bool es_primo (int n) {

    bool primo = false;

    int div = divisores(N);

    if (div == 2)
        primo = true;

    return primo;
}

int main (int argc, char *argv[]) {

    int  div   = divisores(N);
    bool primo = es_primo(N);

    if ( primo == true )
        printf (" %i Es primo\n", N);
    else
        printf (" %i No es primo y tiene %i divisores.\n", N, div);

    return EXIT_SUCCESS;
}



