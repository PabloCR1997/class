#include <stdio.h>
#include <stdlib.h>

#define N 50

int divisor (int n) {

    if ( n < 1 )
        return 0;

    if ( N % n == 0 )
        printf (" %i", n);

    return divisor(n-1);
}

int main (int argc, char *argv[]) {

    printf (" Divisores de %i:\n", N);
    divisor(N);
    printf ("\n");

    return EXIT_SUCCESS;
}



