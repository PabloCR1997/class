#include <stdio.h>
#include <stdlib.h>


int factorial (int n) {
    if ( n <= 0 )
        return 1;

    return n * factorial (n-1);
}

double e (unsigned n) {  
		
	double ERROR = .00001;	
             	            
    double nuevo = 1. / factorial (n);
    if ( nuevo < ERROR )
        return nuevo;   
    return nuevo + e (n+1);
}

int main (int argc, char *argv[]) {

    printf ("e = %.5lf\n", e (0));  

    return EXIT_SUCCESS;
}

