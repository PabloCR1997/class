#include <stdio.h>
#include <stdlib.h>


int suma(unsigned n){
	
	if (n<1)
		return 0;
	
	return n + suma (n-1);
}

int main(int argc, char *argv[]){
	
	unsigned n;
	printf("Indique n: ");
	scanf("%i",&n);
	
	printf("La suma de %i numeros naturales da %i",n,suma(n));
	
	return EXIT_SUCCESS;
	
	
}

