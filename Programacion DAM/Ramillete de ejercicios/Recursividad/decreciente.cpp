#include <stdio.h>
#include <stdlib.h>

#define N 10

void decreciente(int n){
	
	if (n<0)
		return;
		
	printf(" %i",n);
	decreciente(n-1);

}

int main(int argc, char *argv[]){
	
	decreciente(N);
	
	printf ("\n");

	return EXIT_SUCCESS;
	
	
}

