#include <stdlib.h>
#include <stdio.h>

#define N 8

void asc(int n){
	
	if (n<0)
		return;
		
	asc(n-1);
	printf ("%i ",n);	
	
}


int main (int argc, char *argv[]){
	
	asc(N);
	
	printf("\n");
	
	return EXIT_SUCCESS;
}
