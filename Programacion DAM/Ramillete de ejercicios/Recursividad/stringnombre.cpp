#include <stdio.h>
#include <stdlib.h>

int caracter (char *nombre) {

    if ( *nombre == '\0' )
        return 0;

    printf ("%c", *nombre);      
                                
    caracter(nombre+1);          
                                
    return 0;
}

int main (int argc, char *argv[]) {

    char nombre[] = "PablitoCalderon";

    caracter(nombre);
    printf ("\n");

    return EXIT_SUCCESS;
}

