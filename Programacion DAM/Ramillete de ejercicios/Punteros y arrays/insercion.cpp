#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 6
#define M 20



void muestra (int *a[N]) {
    for (int i=0; i<N; i++)
        printf ("%i ", *a[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int A[N];
    int *o[N];
    int menor;

   
    srand (time (NULL));
    for (int i=0; i<N; i++)
        A[i] = rand () % M + 1;   

    for (int i=0; i<N; i++) 
       
        o[i] = &A[i];

    muestra (o);
   

    for (int buscando=0; buscando<N-1; buscando++) {
        menor = buscando;
        for (int i=buscando+1; i<N; i++)
            if (*o[i] < *o[menor])
                menor = i;
        if (menor > buscando) {
            int *aux = o[buscando];
            o[buscando] = o[menor];
            o[menor] = aux;
        }
    }
   
    muestra (o);


    return EXIT_SUCCESS;
}

