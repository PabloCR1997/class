#include <stdio.h>
#include <stdlib.h>


double area_triangulo (double base, double altura) {

    return (base*altura)/2;
}

double area_rectangulo (double base, double altura) {

    return base*altura;
}

int main (int argc, char *argv[]) {


    double base, altura, resultado;
    int opt;

    printf (" \
    Calcular area de:\n\
    \t1-> Tringulo\n\
    \t2-> Rectangulo\n\
    \n\tRespuesta: ");
    scanf (" %i", &opt);

    if ( opt != 1 && opt != 2 ) {
        fprintf (stderr, "\n\tOpcion no permitida.\n");
        exit(1);
    }

    printf("\n\tBase: ");
    scanf(" %lf", &base);

    printf("\tAltura: ");
    scanf(" %lf", &altura);


    double (*func[]) (double, double) = {&area_triangulo, &area_rectangulo};

    printf ("\n\tEl area del %s es %.2lf\n", opt==1 ? "tringulo" :
                                             base!=altura ? "rectangulo" : "cuadrado",
                                             func[opt-1](base, altura) );


    return EXIT_SUCCESS;
}


